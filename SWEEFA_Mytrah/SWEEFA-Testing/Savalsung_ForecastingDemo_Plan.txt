1/29/2018

Things to do [Office]:
1. Apply for Reimbursement for the NIWE Workshop spending(Fill the reimbursement form from Ram attach the approval mail and all evidence and submit)
2. Pass the manually pre-processed Salsung Data file through Data Pre-Post Application for cleaning as it has missing data 
3. Convert Resolution of 10min to 15min for forecasting Purposes 
4. Run Wind Energy Estimation with both 10 min and 15min resolution files
5. Savalsung Energy Estimation - on both 10-15minute resolution files